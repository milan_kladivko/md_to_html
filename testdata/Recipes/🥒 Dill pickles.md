# The quick
[Original recipe](https://www.pantsdownapronson.com/lacto-fermented-cucumbers/#genesis-content)
## Ingredients
1300 g cucumbers
1400 g water
41 g (3 T) salt - pure non-iodized

---
*Optionals*
8 each garlic cloves
4 g (1 teaspoon) mustard seeds
4 g (1 teaspoon) black peppercorns
½ bunch (1 cup) dill blossom or fresh dill

## Process
>🧼 Wash **everything**

1.  Wash the **cucumbers**
2. *Weigh the cucumbers and water:*
	1. Place a glass jar or plastic bucket onto the scales and zero the weight.
	2.  Add the **cucumbers** so that they neatly fill the container.
	3.  Fill the container up with **water**.
3.  Multiply that number by **0.015** - *That is the amount of **salt** needed in the recipe.* 
4. Pour the water out into a container and mix in the **salt** until fully dissolved.
5. Add your **optional spices**.
7.  Pour this **brine** back into the vessel with **cucumbers**
8. Place a small plate on top or a plastic bag filled with water. *This is to make sure the cucumbers stay submerged in the brine.*
9. Close the container with a lid, leaving 4cm room at the top of the vessel.
10. Let the cucumbers ferment at room temperature (21 degrees Celsius) for 5 to 12 days or until you are happy with their taste.




## Pickling vs (lacto)fermenting
> ...**pickling involves putting food into an acidic brine to produce a sour flavor,** whereas **fermenting gives food a sour flavor without any added acid**. Pickling is often the least healthy choice in terms of these two foods.

> Fermented cucumbers are healthy, while store-bought pickles made with vinegar, are not. The fermented version has live probiotic cultures in it while the pickle has none.

> Because store-bought pickles contain sugar as well as pasteurised vinegar. They have zero health benefit to you.
https://www.pantsdownapronson.com/lacto-fermented-cucumbers/#genesis-content


### Brine
Weight of salt needed =  **(Weight of water needed to completely cover ingredients + weight of ingredients) X 0.015**
a
#### Ingredients
![[Pasted image 20220717124105.png]]

-   **Cucumbers** – Use fresh, crisp and preferably seasonal medium-sized cucumbers about 10cm or 5 inches long and 3-4cm or 1,5 inches in diameter.
-   **Water** – Good quality clean drinkable water.
-   **Salt** – Natural salt. Don’t use iodised as it contains some mild anti-bacterial properties. We need bacteria for this to work.
-   **Dill** – I used fresh dill blossom but you can easily replace it with fresh dill.
-   **Garlic** – Use the freshest, nicest and juiciest garlic you can find.
-   **Spices(optional)** – I used whole black mustard seeds but, you can use regular or just leave it out. If you want you could also add things like bay leaf, all-spice, juniper, dill seeds or chilli flakes. You could also add fresh chilli if you like.

#### Process
![[Pasted image 20220717124236.png]]

> ⚠ Make sure to wash the container, your hands and utensils properly to avoid any unwanted bacteria from joining the party.

1.  Wash the cucumbers under cold running water. Place a large 3L glass jar or plastic bucket onto the scales and zero the weight.
2.  Add the cucumbers so that they neatly fill the container.
3.  Fill the container up with water taking note of the total weight of the ingredients and the water.
4.  Multiply that number by 0.015. That is the amount of salt needed in the recipe. Pour the water out into a jug or container and mix in the salt until fully dissolved along with the garlic, mustard seeds and dill blossom. Now you have the brine.
5.  Pour this brine back into the vessel with cucumbers and place a small plate on top or a plastic bag filled with water. This is to make sure the cucumbers stay submerged in the brine.
6.  Close the container with a lid, leaving 4cm or 2 inches room at the top of the vessel,  and let the cucumbers ferment at room temperature (21 degrees Celsius or 69 degrees Fahrenheit) for 5 to 12 days or until you are happy with their taste.

### Notes
-   The time differs as temperature differs from place to place and time of day. The general rule is. The hotter, the quicker it ferments. So, give it a taste after about 3 days and then every few days after that until you are happy with the amount of sourness.
-   I give you this extra in-depth knowledge because your vessel might be different and you might use differently sized cucumbers as well as ferment at different temperatures. Never let your ferment go above 28C or 82F. It will ferment too quickly and produce strange flavours.
-   Wild fermentation, like this, is something you’ll learn over time and the more you ferment the more you will get the hang of it. It’s that simple.
-   See post for more information on storage and troubleshooting


# Other links
https://www.seriouseats.com/lacto-fermented-dill-pickle-recipe
https://tasteofartisan.com/fermented-dill-pickles/
https://www.feastingathome.com/fermented-pickles/
