# Tomato basil soup
A very simple soup made out of, you guessed it, tomatoes and f r e s h basil.
[orig](https://www.theseasonedmom.com/tomato-basil-soup/#wprm-recipe-container-76868)

# The quick
## Ingredients
1 T olive oil
1 chopped onion
2 cloves minced garlic

3x 400g cans tomatoes, NOT drained
0.5 L broth
2 T brown sugar
¼ cup chopped fresh basil
2 bay leaves *or nah - I don't think they make a difference. bay leaves are a lie. and you have to fish them out afterwards, which is pissy to do*

Salt & pepper, to taste
60 ml cream or half-and-half

## Instructions
> Your pot should be at least 2 L, based on the amount of liquid ingredients.
1. Lightly fry **onion and garlic**, 3-5 min
2. Add **tomatoes,** **broth,** brown **sugar,** **basil,** and **bay leaves**
3. Simmer on low for *15 minutes*
4. Remove **bay leaves** from pot. Use an immersion *blender* until desired smoothness.
5. Stir in the **cream** until well combined, adjust **salt & pepper**.

5. Serve: pour [[🌶 Chilli oil]] upon (if using my homemade one, I like to also scoop the chilli seeds and garlic into my soup - the garlic is tender and sweet, and the seeds are crunchy), decorate with a cute sprig of basil, add cheese or [[Maggits]]


#### Sneky's serving recommendation
In order to make this a proper meal, I eat this soup with little cubes of cheese (Gouda, Eidam, etc.).
It's a bit difficult for them not to melt in the soup/be cold in the middle tho, so I do the following:
- take the cheese out of the fridge well ahead so it warms up
- put the cheese cubes in little by little as you're eating so they warm up just enough
- make them as small as you can tolerate so they don't end up with a cold center

# The nitty-gritty
#### Oil
You can use any fat (olive, lard, butter, coconut) but mind that it might ever-so sligtly affect the final taste.
Don't actually measure the amount - just use enough to coat your pot of choice, or more, if your fat is healthy.

#### Onion
Just chop it somehow. Experts say the finer you dice it, the better the flavour, but do you *actually* care about that? Didn't think so.

#### Garlic
Same as above. Also you can put as much garlic as you want, it will get plenty of cooking time so don't worry about the soup being too sharp. Too much garlic? Never heard of it.

#### Bay leaves
Pointless.

#### Tomatoes
**If you can/don't mind using an immersion blender** (or a similar crushing tool) then *canned, peeled, whole tomatoes* are allegedly the best for flavour. Otherwise feel free to use pureed tomatoes or smth like that.

#### [[Broth]]
This is the healthy, collagen-filled bit of the soup. If you're not using a good quality (ideally homemade ofc) broth, this soup is mostly just a delicious snack.

You can either skip it entirely and just add the same amount of water, or water+a boullion cube thing. Feel free to adjust the amount of water at any time to get a thicker / thinner soup.

Now the problematic nature of homemade broth is (at least for me) that you never know how reduced (how much water you cooked off) your broth is compared to other people (or even your past broths).

So I just eyeball it.

My broth usually reaches the jelly form so I just scoop several tablespoons of the stuff ¯\\_(ツ)_/¯
I don't think you can go wrong here.

#### (Brown) Sugar
It adds a lot and it's not really a health-significant (*for how fatty this soup is*) amount unless you're hardcore about your sugar intake (in that case you have no business eating something as sweet as tomatoes anyway).

As for the sugar being brown - it *does* make *a difference*, but don't bother if you're trying to save money or don't have any at home. The taste will only be a bit brighter or whatever, and less.. complex?

You could also use normal sugar + about a teaspoon of *molasses* because that's all that brown sugar is. Molasses are cool to have around as they don't go bad for a long time even when kept in the pantry. They may seem expensive, but recipes usually don't use that much.

#### Basil
The amount is a bit hard to convey, it's like a big handful of the leaves? But again - too much? Never heard of it.

Yeah fresh is best but you may substitute like a Tablespoon or more of the dried stuff instead.

#### Cream
Use the fattiest you can find (at least 30% cmon). As for the amount - more will make the flavour milder and milkier. Too much? Yeah I've been there...
