2022-07-03  13:21

# Foreword
You know when you fry something real well to get it nice and brown, and then you're left with a carpet of fatty brown stuff that's gonna be hell to scrub off with a sponge?

Well what if I told you it's potentially delicious and you should eat it?

### Benefits of pan sauce
1. Extra flavour
2. Easy pan cleaning
3. No waste

### The magic of it
![[Deglazing#^6a4832]]

This is called ***[[Deglazing]]***

### The philosophy
You see, the pan sauce isn't a recipe, it's a  *c o n c e p t*


## Ingredients
### [[The golden trio]]
![[The golden trio#^5f64c6]]

### Tomato paste
Also a universal flavour thing, fits anywhere. The more you fry it, the more rich flavour will develop, probably.

### Spices
Here are my go-to spices that tend to fit anywhere:
- your favourite generic curry mix
- madras curry
- [[Garam masala]] homemade or store-bought
- turmeric
- paprika
- chilli, cayenne

You can put anything you like but it might not "fit" - experiment!

### Veggies

### Flavourful liquid


# Health

# The Recipe
## Recommended ingredients
- onion
- garlic
- *ginger*

- tomato paste
- spices

- veggies

- flavourful liquid

## Process
1. fry the onion until it's soft, adding the garlic and ginger about halfway
2. add tomato paste and spices and fry until you're scared it's gonna burn
3. add your flavourful liquid and *deglaze* -- scrape the bottom until it all gets dissolved

# Tips