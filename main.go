package main

import (
	"flag"
	"log"

	"io"
	"io/fs"
	"os"
	fp "path/filepath"
	str "strings"

	"bytes"

	"github.com/yuin/goldmark"
	"github.com/yuin/goldmark/extension"
	"github.com/yuin/goldmark/renderer/html"
)

type file struct {
	path_in  string
	path_out string

	path_rooted string

	md   string
	html string

	wasWritten bool
}

var (
	dirMarkdown = flag.String(
		"mdir", ".",
		"Directory with markdown files.")
	dirOutput = flag.String(
		"out", "./html",
		"Output html files.")
)

func main() {

	//
	//  Program Args
	//

	flag.Parse()
	log.Printf("Reading Markdown directory:\n%s (%s)", abs(*dirMarkdown), *dirMarkdown)
	log.Printf("Output HTML directory:\n%s (%s)", abs(*dirOutput), *dirOutput)
	log.Printf("Output HTML directory:\n%s (%s)", abs(*dirOutput), *dirOutput)

	//
	//  Grab the filenames with walking the folder
	//
	var theFiles []file
	{
		var collectMarkdowns fs.WalkDirFunc = func(
			path string, info fs.DirEntry, err error,
		) error {
			if info.IsDir() || str.ToLower(fp.Ext(path)) != ".md" {
				return nil
			}

			rooted := path
			if *dirMarkdown != "." {
				// strip the input dir prefix,
				// but only if it's not the default
				rooted = str.Replace(path, *dirMarkdown, "", 1)
				rooted = str.TrimLeft(rooted, string(fp.Separator))
			}

			out := fp.Join(*dirOutput, rooted)        // prefix with output dir instead
			out = str.Replace(out, ".md", ".html", 1) // we'll be writing html files

			theFiles = append(theFiles, file{
				path_in:     path,
				path_out:    out,
				path_rooted: rooted,
			})
			return nil
		}
		err := fp.WalkDir(*dirMarkdown, collectMarkdowns)
		if err != nil {
			log.Fatalf("[ERROR] couldn't walk markdown dir: %v", err)
			return
		}
	}

	if len(theFiles) == 0 {
		log.Fatalf("[ERROR] no markdown files found in %s", *dirMarkdown)
	} else { // log
		{
			var sb str.Builder
			for _, f := range theFiles {
				sb.WriteString("- ")
				sb.WriteString(f.path_in)
				sb.WriteString("\n")
			}
			log.Printf("Found %d markdown files: \n%s", len(theFiles), sb.String())
		}
		{
			var sb str.Builder
			for _, f := range theFiles {
				sb.WriteString("- ")
				sb.WriteString(f.path_out)
				sb.WriteString("\n")
			}
			log.Printf("Will write them as: \n%s", sb.String())
		}
	}

	//
	//  Reading files and grabbing the content
	//
	log.Printf("Reading the file contents...")
	for i := range theFiles {
		f := &theFiles[i] // modify array
		file, err := os.Open(abs(f.path_in))
		if err != nil {
			log.Printf("[WARN] couldn't open file: %s", err)
			continue
		}
		bytes, err := io.ReadAll(file)
		if err != nil {
			log.Printf("[WARN] couldn't read file: %s", err)
			file.Close() // can't defer in for loops
			continue
		}
		file.Close() // can't defer in for loops

		f.md = string(bytes)
	}
	log.Printf("Reading done.")

	//
	//  Process the Obsidian-specific features -- set up anchors and links.
	//
	{
		// That might be a big TODO
	}

	//
	//  Convert it all to HTML via a lib
	//
	log.Printf("Converting to HTML...")
	gm := goldmark.New(
		goldmark.WithRendererOptions(html.WithUnsafe()),
		goldmark.WithExtensions(extension.TaskList),
	)
	for i := range theFiles {
		f := &theFiles[i] // modify array
		var bs bytes.Buffer

		err := gm.Convert([]byte(f.md), &bs)
		if err != nil {
			log.Printf("[WARN] markdown error in file %s: %v", f.path_in, err)
			continue
		}

		f.html = bs.String()
	}
	log.Printf("Conversion done.")

	//
	//  Wrap all the pages in templates -- give <head> content and so on
	//
	if false {
		log.Printf("{#{ %s }#}", theFiles[3].html)
	}
	log.Printf("Wrapping into template HTML bodies with styling... ")
	for i := range theFiles {
		f := &theFiles[i] // modify array

		f.html = f.wrappedIntoHTML(theFiles)
	}
	log.Printf("Wraps done.")
	if false {
		log.Printf("{-{ %s }-}", theFiles[3].html)
	}

	//
	//  Write the HTML files
	//
	log.Printf("Writing HTML files...")
	for i := range theFiles {
		f := &theFiles[i] // modify array

		// ensure that we have the directory laid out
		err := os.MkdirAll(fp.Dir(f.path_out), 0755)
		if err != nil {
			log.Printf("[warn] couldn't ensure dir %s: %v",
				fp.Base(f.path_out), err)
			// continue // don't, let it error out properly if it needs to
		}

		// write the file and its html content
		// NOTE:  If file exists, will overwrite
		file, err := os.Create(f.path_out)
		if err != nil {
			log.Printf("[ERROR] couldn't create HTML file \nTO:   %s\nFROM: %s\n: %v",
				f.path_out, f.path_in, err)
			continue
		}
		file.WriteString(f.html)
		file.Close()

		f.wasWritten = true
	}

	{ // log
		var sb str.Builder
		var n = 0
		for _, f := range theFiles { // readonly
			if f.wasWritten == false {
				continue
			}
			n++
			sb.WriteString("+ ")
			sb.WriteString(f.path_out)
			sb.WriteString("\n")
		}
		log.Printf("Wrote %d html files: \n%s", n, sb.String())
	}

	//
	//  Write side-files (stylesheets, fonts, indexes, search options, ...)
	//
	{
		// NOTE:  For now, it's just a stylesheet with some code
		//   so that we don't overwrite anything customized.
		log.Printf("Writing the stylesheet next to content...")

		var stylePath = getStylePath()
		if fileExists(stylePath) {
			withSuffix := appendBeforeExtension(stylePath, ".new")
			log.Printf("[warn]\n"+
				"  Found a file at %s!! \n"+
				"  Will write a different file avoid overwriting it.",
				withSuffix)
			stylePath = withSuffix
		}

		// NOTE:  We're using the compile-time-embedded stylesheet here.
		var styleContent = STYLE
		// NOTE:  If file exists, will overwrite
		file, err := os.Create(stylePath)
		if err != nil {
			log.Printf("[ERROR] couldn't write stylesheet at %s: %v",
				stylePath, err)
		}
		file.Write(styleContent)
		file.Close()

		log.Printf("+ %s", stylePath)
	}
}

// ---------------------------------------------------------------------------------

func init() {
	log.SetFlags(0)     // don't put timestamps in the logs
	log.SetPrefix("\n") // a starting breakline it looks pretty nice in most cases
}

func abs(p string) string {
	abs, err := fp.Abs(p)
	if err != nil {
		return p
	}
	return abs
}

func fileExists(path string) bool {
	_, err := os.Stat(path)
	if err != nil {
		// Will have a `os.ErrNotExist`, but could be other error values
		// I think assuming and not checking is fine.
		return false
	}
	return true // If no error, probably exists
}
func appendBeforeExtension(path, suffix string) string {
	ext := fp.Ext(path)               // includes the dot
	base := path[:len(path)-len(ext)] // part without the ext
	return base + suffix + ext
}
