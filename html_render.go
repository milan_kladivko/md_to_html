package main

import (
	_ "embed" // just for the `//go:embed` lines

	"fmt"
	"path/filepath"
	"strings"
)

// NOTE:  Any embedded files are mentioned with paths relative
//   to this source file they're imported from.
//
//   You can't do it wrong though -- it's a compile-time error
//   if the file cannot be found. But I'm not sure how good
//   the error report is in that case...

//go:embed mdstyles.css
var STYLE []byte

func getStylePath() string { return filepath.Join(*dirOutput, "styles.css") }

//

func (f file) wrappedIntoHTML(others []file) string {

	const __template = `<!doctype html>
        <head>
          <meta charset='utf-8'>
          <meta name='viewport' content='width=device-width,initial-scale=1'>
          <title> {{title}} </title>
          <link rel='stylesheet' href='{{css_fpath}}' />
        </head>
        <body>
          <nav> {{nav}} </nav>
          <main> {{content}} </main>
        </body>`

	t := __template // copy
	repOne := func(t, o, n string) string { return strings.Replace(t, o, n, 1) }
	rep := func(t, o, n string) string { return strings.ReplaceAll(t, o, n) }

	t = rep(t, "{{title}}", filepath.Base(f.path_rooted))
	t = repOne(t, "{{css}}", string(STYLE))
	t = repOne(t, "{{css_fpath}}", rel_FromTo(f.path_out, getStylePath()))
	t = repOne(t, "{{nav}}", navHeader_FromTo(f, others))
	t = repOne(t, "{{content}}", f.html)

	return t
}

func rel_FromTo(from, to string) string {

	// TODO:  Here we might need to do extra stuff when
	//   the files are generated on a Windows machine -- the paths
	//   in the links should then use web-slashes and not Win-backsl.

	// filepath.Rel expects a foldername -- it itself doesn't check
	// if the supplied path is a folder or a filename.
	fromDir := filepath.Dir(from)
	// We need a path to other files relative to the currently viewed
	// file -- Go has a handy function for cleaning filepaths
	// into such forms.
	toRel, relErr := filepath.Rel(fromDir, to)

	if relErr != nil {
		return fmt.Sprintf(":err:%s: %v", to, relErr)
	}
	return toRel
}

func navHeader_FromTo(currentFile file, others []file) string {
	var sb strings.Builder
	w := func(s string) { sb.WriteString(s) }

	w("\n")
	for _, it := range others {
		current := it.path_out == currentFile.path_out

		if current {
			w("<span>")
		} else {
			rel := rel_FromTo(currentFile.path_out, it.path_out)
			w("<a href='" + rel + "' >")
		}

		w("  " + it.path_rooted + "  ")

		if !current {
			w("</a>")
		} else {
			w("</span>")
		}
		w("\n")
	}

	return sb.String()
}
